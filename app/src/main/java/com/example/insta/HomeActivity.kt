package com.example.insta

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.insta.fragments.*
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val homeFragment = HomeFragment()
        val likesFragment = LikesFragment()
        val profileFragment = ProfileFragment()
        val searchFragment = SearchFragment()
        val shareFragment = ShareFragment()
        val bnv = findViewById<BottomNavigationView>(R.id.bottom_navigation)

        makeCurrentFragment(homeFragment)

        bnv.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.home -> makeCurrentFragment(homeFragment)
                R.id.likes -> makeCurrentFragment(likesFragment)
                R.id.profile -> makeCurrentFragment(profileFragment)
                R.id.search -> makeCurrentFragment(searchFragment)
                R.id.share -> makeCurrentFragment(shareFragment)
            }
            true
        }
    }

    private fun makeCurrentFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            commit()
        }
    }
}